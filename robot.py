#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
import numpy as np
from math import cos,sin,asin,atan2,sqrt,pi

#Dimention du robot
m=3    #hauteur de la liaison 2
n=2     #longueur du bras de l'organe terminal

#Vitesses maximales Kvi des i liaisons dans leurs espaces operationels
Kv1=pi
Kv2=.3
Kv3=2/3*pi

#Accelerations maximales Kai des i liaisons dans leurs espaces operationels
Ka1=3*pi
Ka2=1
Ka3=2*pi

#Duree entre deux instructions (en secondes)
step=0.05

#Butes

q1Max=1.9*pi
q2Max=10
q3Max=1.9*pi


def demo():
    A = (4,2,2)
    B = (8,1,4)
    C = (3,0,3)
    vitB = (.2,0,0)
    troisPoints( A, B, C, vitB )


def troisPoints( A, B, C, vitB ):

    (q1A,q2A,q3A) = mgi( A[0], A[1], A[2] ) 
    (q1B,q2B,q3B) = mgi( B[0], B[1], B[2] )
    (q1C,q2C,q3C) = mgi( C[0], C[1], C[2] )
    
    jacobienneB = create_jacobienne(q1B, q2B, q3B)
    (dq1B,dq2B,dq3B) = mdi( vitB[0], vitB[1], vitB[2], jacobienneB )
    print( vitB[2],mdd(dq1B, dq2B, dq3B,jacobienneB)) 
    t1_part1 = toptimum( q1A, 0, 0, q1B, dq1B, 0, Kv1, Ka1)
    t2_part1 = toptimum( q2A, 0, 0, q2B, dq2B, 0, Kv2, Ka2)
    t3_part1 = toptimum( q3A, 0, 0, q3B, dq3B, 0, Kv3, Ka3)
    topti_part1 = np.max([t1_part1,t2_part1,t3_part1])
    
    t1_part2 = toptimum( q1B, dq1B, 0, q1C, 0, 0, Kv1, Ka1)
    t2_part2 = toptimum( q2B, dq2B, 0, q2C, 0, 0, Kv2, Ka2)
    t3_part2 = toptimum( q3B, dq3B, 0, q3C, 0, 0, Kv3, Ka3)
    topti_part2 = np.max([t1_part2,t2_part2,t3_part2])
    
    (t1,q1_part1,dq1_part1,ddq1_part1) = poly( q1A, 0, 0, q1B, dq1B, 0, topti_part1)
    (t1,q2_part1,dq2_part1,ddq2_part1) = poly( q2A, 0, 0, q2B, dq2B, 0, topti_part1)
    (t1,q3_part1,dq3_part1,ddq3_part1) = poly( q3A, 0, 0, q3B, dq3B, 0, topti_part1)
    (t2,q1_part2,dq1_part2,ddq1_part2) = poly( q1B, dq1B, 0, q1C, 0, 0, topti_part2)
    (t2,q2_part2,dq2_part2,ddq2_part2) = poly( q2B, dq2B, 0, q2C, 0, 0, topti_part2)
    (t2,q3_part2,dq3_part2,ddq3_part2) = poly( q3B, dq3B, 0, q3C, 0, 0, topti_part2)
    
    q1 = np.concatenate((q1_part1.T[:-1], q1_part2.T))
    q2 = np.concatenate((q2_part1.T[:-1], q2_part2.T))
    q3 = np.concatenate((q3_part1.T[:-1], q3_part2.T))
    dq1 = np.concatenate((dq1_part1.T[:-1], dq1_part2.T))
    dq2 = np.concatenate((dq2_part1.T[:-1], dq2_part2.T))
    dq3 = np.concatenate((dq3_part1.T[:-1], dq3_part2.T))
    ddq1 = np.concatenate((ddq1_part1.T[:-1], ddq1_part2.T))
    ddq2 = np.concatenate((ddq2_part1.T[:-1], ddq2_part2.T))
    ddq3 = np.concatenate((ddq3_part1.T[:-1], ddq3_part2.T))
    nbPas = t1.shape[0] + t2.shape[0] -1
    
    t = np.arange(0,nbPas*step,step)
    x = np.zeros(t.shape[0])
    y = np.zeros(t.shape[0])
    z = np.zeros(t.shape[0])

    dx = np.zeros(t.shape[0])
    dy = np.zeros(t.shape[0])
    dz = np.zeros(t.shape[0])
    
    ddx = np.zeros(t.shape[0])
    ddy = np.zeros(t.shape[0])
    ddz = np.zeros(t.shape[0])
    

    for m in range(q1.shape[0]):
        (q1m,q2m,q3m) = (q1.T[m],q2.T[m],q3.T[m])
        if not checkBorne( q1m, q2m, q3m ):
            print("erreur: les parametres saisis sont invalides", file=sys.stderr)
            return
        
        (dq1m,dq2m,dq3m) = (dq1.T[m],dq2.T[m],dq3.T[m])
        (x[m],y[m],z[m]) = mgd(q1m,q2m,q3m)
        jqm = create_jacobienne(q1m, q2m, q3m)
        (dx[m],dy[m],dz[m]) = mdd(dq1m,dq2m,dq3m,jqm)
        (ddx[m],ddy[m],ddz[m]) = mad(q1m,q2m,q3m,
                dq1.T[m],dq2.T[m],dq3.T[m],
                ddq1.T[m],ddq2.T[m],ddq3.T[m],jqm)
    
    vitAbs= (dx**2 + dy**2 + dz**2)**.5
    
    fig = plt.figure()
    ax = fig.add_subplot(121,projection='3d')
    ax.scatter( x, y, z, c='b', marker='.')
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    ax = fig.add_subplot(122,projection='3d')
    ax.scatter( q1, q2, q3, c='b', marker='.')
    ax.set_xlabel("q1")
    ax.set_ylabel("q2")
    ax.set_zlabel("q3")
    plt.show(block=False)

    plt.figure()
    plt.plot(t,vitAbs,"r-")
    plt.title("vitesse")
    plt.show(block=False)
    
    
    affiche3courbes("x y z",x,y,z,t)
    affiche3courbes("vitesse x y z",dx,dy,dz,t)
    affiche3courbes("acceleration x y z",ddx,ddy,ddz,t)
    affiche3courbes("q1 q2 q3",q1,q2,q3,t)
    affiche3courbes("vitesse de q1 q2 q3",dq1,dq2,dq3,t)
    affiche3courbes("acceleration en q1 q2 q3",ddq1,ddq2,ddq3,t)
    
def affiche3courbes(nom,f,fd,fdd, t):
    plt.figure()

    plt.subplot(311)
    plt.plot(t, f, "r-")
    plt.xlabel('Temps(s)')
    plt.grid(True)
    plt.title('Affichage des courbes fonction de ' + nom)
    plt.subplot(312)
    plt.plot(t, fd, "r-")
    plt.xlabel('Temps(s)')
    plt.grid(True)
    plt.subplot(313)
    plt.plot(t, fdd, "r-")
    plt.xlabel('Temps(s)')
    plt.grid(True)
    
    plt.show(block=False)
    
#Verifie que les qi soit dans les bornes
def checkBorne( q1, q2, q3 ):
    return abs(q1)<q1Max and q2<q2Max and abs(q3)<q1Max

#permet de generer les courbes de position, vitesse et acceleration d'une liaison dans l'espace operationel
# pos0,vit0,acc0 -> position, vitesse, acceleration au depart de la course
# posF,vitF,accF -> position, vitesse, acceleration a la fin de la course
def poly( pos0, vit0, acc0, posF, vitF, accF, tf):
    inArray = np.array([pos0,vit0,acc0,posF,vitF,accF])
    
    mat = np.array([
            [0,0,0,0,0,1],
            [0,0,0,0,1,0],
            [0,0,0,2,0,0],
            [tf**5, tf**4, tf**3, tf**2, tf, 1],
            [5*tf**4, 4*tf**3, 3*tf**2, 2*tf, 1, 0],
            [20*tf**3, 12*tf**2, 6*tf, 2, 0, 0]])
    
    (a,b,c,d,e,f)=np.matmul(np.linalg.inv(mat),inArray.T)
    pos = lambda t : a*t**5 + b*t**4 + c*t**3 + d*t**2 + e*t + f
    vit = lambda t : 5*a*t**4 + 4*b*t**3 + c*3*t**2 + 2*d*t + e
    acc = lambda t :  20*a*t**3 + 12*b*t**2 + 6*c*t + 2*d
   
    t = np.arange( 0, tf, step )
    
    return ( t, pos(t), vit(t), acc(t) )

#permet de trouver le temps optimal pour effectuer la trajectoire de la liaison en respectant les contraintes de vitesse et d'acceleration 
def toptimum( pos0, vit0, acc0, posF, vitF, accF, vitMax, accMax):
    tf=1
    coef=1
    while True:
        tf = tf*coef
        (t,p,v,a) = poly( pos0, vit0, acc0, posF, vitF, accF, tf)
        currentVitMax = np.max(np.abs(v))/vitMax
        currentAccMax = np.max(np.abs(a))/accMax
        coef = np.max([currentVitMax,currentAccMax**.5])
        
        if (0.95<coef and  coef<1.05)  or tf*coef<1e-20:
            return tf

    
def mgd( q1, q2, q3 ):

    
    R01 = np.array([
        [cos(q1), -sin(q1), 0, 0 ],
        [sin(q1), cos(q1),  0, 0 ],
        [0, 0, 1, m],
        [0,0,0,1]])

    R12 = np.array([
        [0,-1,0,0],
        [0,0,-1,q2],
        [1,0,0,0],
        [0,0,0,1]])

    R23 = np.array([
        [cos(q3),sin(q3),0 , 0],
        [0,0,-1,0],
        [sin(q3),-cos(q3),0,0],
        [0,0,0,1]])

    R45 = np.array([ 0, n, 0, 1])
    return  np.matmul(np.matmul(np.matmul(R01, R12), R23),R45) [:-1]


    
    
def mgi(x,y,z):
    
    if z<m-n:
        sys.exit("Erreur : le parametre z est trop petit")
    if z>m+n:
        sys.exit("Erreur : le parametre z est trop grand")
        
    q = [0,0,0]
    if y != 0:
        q1= -atan2( x, y)
    else:
        if x<0:
            q1=pi/2
        else:
            q1=-pi/2
    
    q3=(asin((z-m)/n))
    
    if x**2+y**2>=(cos(q3)*n)**2:
        q2=sqrt(x**2+y**2)-cos(q3)*n
    else:
        q3= (pi - q3)
        q2=sqrt(x**2+y**2)-cos(q3)*n
        
    return (q1,q2,q3)
    
def create_jacobienne(q1, q2, q3):
    Jq = np.array([
        [-q2*cos(q1)-n*cos(q3)*cos(q1), -sin(q1), n*sin(q3)*sin(q1)],
        [-q2*sin(q1)-n*cos(q3)*sin(q1), cos(q1), -n*sin(q3)*cos(q1)],
        [0, 0, n*cos(q3)]])
    return Jq
    
def mdd(dq1, dq2, dq3, Jq):
    dQ = np.array([dq1,dq2,dq3]) 
    dX = np.dot(Jq,dQ)
    return dX

    
def mdi(dx, dy, dz, Jq):
    dX = np.array([dx,dy,dz])
    Jq_inv = np.linalg.pinv(Jq)
    dQ = np.dot(Jq_inv, dX)
    return dQ
    
def mad(q1,q2,q3,dq1,dq2,dq3,dqq1,dqq2,dqq3,Jq):
    dJq = np.array([
        [-dq2*cos(q1)+q2*sin(q1)*dq1+n*sin(q3)*cos(q1)*dq3+n*cos(q3)*sin(q1)*dq1, -cos(q1)*dq1, n*cos(q3)*sin(q1)*dq3+n*sin(q3)*cos(q1)*dq1],
        [-q2*cos(q1)*dq1-dq2*sin(q1)+n*sin(q3)*sin(q1)*dq3-n*cos(q3)*cos(q1)*dq1, -sin(q1)*dq1, -n*cos(q3)*cos(q1)*dq3+n*sin(q3)*sin(q1)*dq1],
        [0, 0, -n*sin(q3)*dq3]])
    dQ = np.array([[dq1],[dq2],[dq3]])
    ddQ = np.array([[dqq1],[dqq2],[dqq3]])
    ddX = np.dot(Jq,ddQ) + np.dot(dJq,dQ)
    return ddX
