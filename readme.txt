Le programme fonctionne avec python3

> python3 -i BE_GT4_2.py

vous pouvez maintenant appeler la fonction demo()

> demo()


Cette fonction sert a utiliser le prototype avec des valeurs pre-entree.
Ces points sont A=(4,2,2) B=(8,1,4) C=(3,3,3) Bvit=(.1,0,0)

Vous pouvez appeler vous meme le prototype :

> troisPoints( A=(3,4,5), B=(5,5,5), C=(5,4,3), Bvit=(.1,.2,0) )

Pour modifier les parametres, il suffit de modifier les variables globales au debut du fichier python
Vous pouvez modifier:
- Les dimensions du robot
- Les vitesses maximales Kvi des i liaisons dans leurs espaces operationnels
- Les accelerations maximales Kai des i liaisons dans leurs espaces operationnels
- Les durees entre deux instructions
- Les butées
